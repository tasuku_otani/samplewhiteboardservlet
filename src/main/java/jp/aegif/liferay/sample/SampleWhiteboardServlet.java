package jp.aegif.liferay.sample;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"osgi.http.whiteboard.servlet.name=jp.aegif.liferay.sample.SampleWhiteboardServlet",
		"osgi.http.whiteboard.servlet.pattern=/get-email-address-hash"
	},
	service = Servlet.class
)
public class SampleWhiteboardServlet extends HttpServlet {
	
	private static final String API_PRIVATE_KEY = "change_it";
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// sample implementation to generate SHA-1 hash of Munchkin API Private Key + Email Address
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter printWriter = response.getWriter();
		try {
			String source = API_PRIVATE_KEY + Objects.toString(request.getParameter("email"), "");
			byte[] bytes = MessageDigest.getInstance("SHA-1").digest(source.getBytes(StandardCharsets.UTF_8));
			printWriter.print(DatatypeConverter.printHexBinary(bytes));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		printWriter.close();
	}
}